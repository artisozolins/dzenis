<?php
include('functions.php');
$file = fopen($_FILES['report']['tmp_name'], 'r');
$header = fgetcsv($file, null, ',');

/* Assign data to key - value pairs */
$data = [];
$i = 0;

while ($row = fgetcsv($file, null, ',')) {
    foreach ($row as $key => $field) {
        $data[$i][getKey($header[$key])] = $field;
    }
    $i++;
}

$results = [];

usort($data, 'compareWorkRows');

foreach ($data as $row) {
    if (!isset($results[$row['issue_key']])) {
        $results[$row['issue_key']] = [
            'time_spent' => 0,
            'work_descriptions' => []
        ];
    }

    $results[$row['issue_key']]['issue_summary'] = $row['issue_summary'];
    $results[$row['issue_key']]['time_spent'] += getNumber($row['hours']);
    $results[$row['issue_key']]['work_descriptions'][] = $row['work_description'];
}

include('index.php');