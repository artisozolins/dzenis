## Dzeņa README ##


# Dzenis is a script to make a formatted report from JIRA CSV file #

# Usage #

1) Select date in JIRA timesheet for which you want to create the report
 ![Selection_123.png](https://bitbucket.org/repo/jby899/images/1576146655-Selection_123.png)

2) Press export to Excel
  ![Selection_124.png](https://bitbucket.org/repo/jby899/images/4046300059-Selection_124.png)

3) Open XLSX file and save it as CSV file using comma as delimiter

4) Open Dzenis page, upload the CSV file and get the report.
   Ctrl+C and Ctrl+C to report spreadsheet.
![Selection_125.png](https://bitbucket.org/repo/jby899/images/25036437-Selection_125.png)