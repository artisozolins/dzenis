<?php

/**
 * @param  string $value
 * @return bool
 */
function getKey($value)
{
    return str_replace(' ', '_', strtolower($value));
}

/**
 * @param  array $row
 * @return bool
 */
function isDedicated($row)
{
    return strpos($row['issue_key'], 'DD-') !== false;
}

/**
 * @param  array $row
 * @return bool
 */
function isTask($row)
{
    return $row['issue_type'] == 'Task' || isDedicated($row);
}

/**
 * @param  array $row
 * @return bool
 */
function isBug($row) {
    return $row['issue_type'] == 'Bug';
}

/**
 * @param  array $row
 * @return bool
 */
function isCommunication($row) {
    return strpos(strtolower($row['issue_summary']), 'communication') !== false;
}

/**
 * @param  string $value
 * @return float
 */
function getNumber($value)
{
    return (float) str_replace(',', '.', $value);
}

/**
 * @param  string $a
 * @param  string $b
 * @return int
 */
function compareWorkRows($a, $b)
{
   return strtotime($a['work_date']) - strtotime($b['work_date']);
}

/**
 * @param array $data
 */
function getReportString($data)
{
    $result = '';

    foreach ($data as $issueKey => $row) {
        $result .= "\n- " . $issueKey . " " . $row['issue_summary'] . " - " . number_format($row['time_spent'], 2) . "h";

        foreach ($row['work_descriptions'] as $workDescription) {
            $result .= "\n    - " . str_replace("\n", ' ', $workDescription);
        }
    }

    return $result;
}