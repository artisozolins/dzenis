<!doctype html>
<html xml:lang="lv" lang="lv">
<head>
    <meta charset="utf-8">
    <title>Dzenis</title>
    <style type="text/css">
        body { padding:40px  20px; font-family: 'Monospace'; }
        img { float:left; margin:40px 0 0 -73px; }
        .form-container { float:left; padding-top:60px; position:relative; margin-bottom: 110px; }
        .results { float:left; background: #ccc; width: 100%; padding: 20px; min-height: 400px; }
        .report-label { clear: both; display: block; }

        img {
            -webkit-transition: margin 1s ease;
            -moz-transition: margin 1s ease;
            -o-transition: margin 1s ease;
            -ms-transition: margin 1s ease;
            transition: margin 1s ease;
        }

        img:hover {
            margin-top: -500px;
        }


    </style>
</head>
<body>
<div class="form-container">
    <form id="form" enctype="multipart/form-data" method="post" action="generate.php" onsubmit="if (!document.getElementById('file').value) { alert('Norādi JIRA csv failu!'); return false; }">
        <label for="file">JIRA CSV fails --> </label>
        <input id="file" type="file" name="report"/>
        <button type="submit">Augšupielādēt</button>
    </form>
</div>
<img src="logo.gif" width="200"/>
<?php if (isset($results)): ?>
    <label class="report-label" for="report">Atskaite:</label>
    <textarea id="report" class="results"><?php echo getReportString($results) ?></textarea>
<?php endif ?>
</body>
</html>